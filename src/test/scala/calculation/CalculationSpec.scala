package calculation

import org.scalatest._

class CalculationSpec extends FlatSpec with Matchers {
  "Calculation somme2" should "return a number" in {
    Calculation.somme2(2, 2) shouldEqual 4
  }

  "Calculation somme2" should "return a right result" in {
    Calculation.somme2(3, 2) shouldEqual 5
  }

  "Calculation somme3" should "return a right result" in {
    Calculation.somme3(3, 2, 1) shouldEqual 6
  }
}
