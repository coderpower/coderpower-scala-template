package calculation

object Calculation extends App {
  def somme2(x: Int, y: Int) = x + y
  def somme3(x: Int, y: Int, z: Int) = x + y + z
}
